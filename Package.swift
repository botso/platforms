// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Platforms",
    products: [
        .library(
            name: "Platforms",
            targets: ["Platforms"]),
    ],
    dependencies: [
      .package(name: "Domain", url: "https://gitlab.com/bitso1/domain", from: "0.0.1"),
    ],
    targets: [
        .target(
            name: "Platforms",
            dependencies: ["Domain"]),
    ]
)
