//
//  File.swift
//  
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation
import Domain
public struct UseCaseProviderIMP: UseCaseProvider {
  
  public init() {}
  
  public func getHomeUseCases() -> HomeUseCases {
    return HomeUseCasesIMP()
  }
  
  public func getBookDetailsUseCases() -> BookDetailsUseCases {
    return BookDetailsUseCasesIMP()
  }
}
