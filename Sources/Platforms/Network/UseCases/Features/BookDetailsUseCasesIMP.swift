//
//  BookDetailsUseCasesIMP.swift
//  
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation
import Domain

public struct BookDetailsUseCasesIMP: BookDetailsUseCases {
  public func getDetails(request: BookDetailIO.Request, completion: @escaping (Result<BookDetailIO.Response, AppError>) -> Void) -> URLSessionDataTask {
    if let ticker = MetaDataLoader.instance.tickerCache[request.book] {
      completion(Result.success(ticker))
    }
    return CoreNetwork().get(url: Endpoints.PublicRestAPI.ticker(request.book).url, objectType: BookDetailIO.Response.self, completion: { res in
      switch res {
      case .success(let data):
        MetaDataLoader.instance.tickerCache.insert(data, forKey: request.book)
        completion(Result.success(data))
      case .failure(let error):
        completion(Result.failure(error))
      }
    })
  }
}
