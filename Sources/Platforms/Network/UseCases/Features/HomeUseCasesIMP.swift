//
//  File.swift
//  
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation
import Domain
public struct HomeUseCasesIMP: HomeUseCases {
  public func getBooks(completion: @escaping (Result<[Book], AppError>) -> Void) -> URLSessionDataTask {
    return CoreNetwork().get(url: Endpoints.PublicRestAPI.availableBooks.url, objectType: [Book].self, completion: { res in
      switch res {
      case .success(let data):
        completion(Result.success(data))
      case .failure(let error):
        completion(Result.failure(error))
      }
    })
  }
}
