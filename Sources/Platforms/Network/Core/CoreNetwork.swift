//
//  CoreNetwork.swift
//  
//
//  Created by Behrad Kazemi on 6/16/22.
//

import Foundation
import Domain

public struct CoreNetwork {
  let timeout = 60.0
  public init() {}
  public func get<T: Codable>(url: String, objectType: T.Type, completion: @escaping (Result<T, AppError>) -> Void) -> URLSessionDataTask {
    let dataURL = URL(string: url)!
    let session = URLSession.shared
    let request = URLRequest(url: dataURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: timeout)
    return session.dataTask(with: request, completionHandler: { data, response, error in
      guard error == nil else {
        completion(Result.failure(AppError.networkError(error!)))
        return
      }
      
      guard let data = data else {
        completion(Result.failure(AppError.dataNotFound))
        return
      }
      
      do {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.iso8601Full)
        let decodedObject = try decoder.decode(BaseResponse<T>.self, from: data)
        if decodedObject.success {
          completion(Result.success(decodedObject.payload!))
          return
        }
        guard let responseError = decodedObject.error else { return }
        completion(Result.failure(AppError.networkError(NSError(domain: responseError.message, code: Int(responseError.code)!, userInfo: nil))))
      } catch let error {
        completion(Result.failure(AppError.jsonParsingError(error as! DecodingError)))
      }
    })
  }
}
