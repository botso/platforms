//
//  MetaDataLoader.swift
//  Domain
//
//  Created by Behrad Kazemi on 8/21/20.
//  Copyright © 2020 Behrad Kazemi. All rights reserved.
//

import Foundation
import Domain

public final class MetaDataLoader {
  
  public static let instance = MetaDataLoader()
  
  let tickerCache = Cache<String, BookDetailIO.Response>()
  
}
